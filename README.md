    Image Base64 uploader
=====================
Expansion for working with pictures in base64 (module, model, etc.)

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist xtetis/yii2-imagesbuploader "*"
```

or add

```
"xtetis/yii2-imagesbuploader": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \xtetis\imagesuploader\AutoloadExample::widget(); ?>```



4. Миграции модуля xtetis/yii2-imagesbuploader

    ./yii migrate/up --migrationPath=@vendor/xtetis/yii2-imagesbuploader/src/migrations











The extension has been generated successfully.

To enable it in your application, you need to create a git repository and require it via composer.

cd /var/www/ukrbix.lc/framework/runtime/tmp-extensions/yii2-imagesbuploader

git init
git add -A
git commit
git remote add origin https://path.to/your/repo
git push -u origin master
The next step is just for initial development, skip it if you directly publish the extension on packagist.org

Add the newly created repo to your composer.json.

"repositories":[
    {
        "type": "git",
        "url": "https://path.to/your/repo"
    }
]
Note: You may use the url file:///var/www/ukrbix.lc/framework/runtime/tmp-extensions/yii2-imagesbuploader for testing.

Require the package with composer

composer.phar require xtetis/yii2-imagesbuploader:dev-master
And use it in your application.

\xtetis\imagesuploader\AutoloadExample::widget();
When you have finished development register your extension at packagist.org.
<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%image}}`.
 */
class m190414_062800_create_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'id_album' => $this->integer()->notNull(),
            'is_main' => $this->integer()->notNull(),
            'md5' => $this->string(50),
            'src' => $this->string(300),
        ]);

        $this->addCommentOnColumn('{{%image}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%image}}','id_album', 'Альбом');
        $this->addCommentOnColumn('{{%image}}','is_main', 'Изображение заглавное для альбома');
        $this->addCommentOnColumn('{{%image}}','md5', 'md5 картинки');
        $this->addCommentOnColumn('{{%image}}','src', 'Путь к картинке');

        $this->addCommentOnTable('{{%image}}','Список изображений');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%image}}');
    }
}

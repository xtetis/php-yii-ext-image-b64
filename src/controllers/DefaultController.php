<?php

namespace xtetis\image\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use xtetis\imagesuploader\ModuleTrait;


/**
 * ImageController implements the CRUD actions for Image model.
 */
class DefaultController extends Controller
{



    /**
     * Lists all Image models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

}

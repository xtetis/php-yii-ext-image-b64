<?php
use yii\web\View;


?>
<div id="xtetis_imagesuploader"
     input_name="<?=$input_name?>"
     images_limit="<?=intval($model->images_limit)?>"
     model_name="<?=\yii\helpers\StringHelper::basename(get_class($model))?>">
    <!-- Область для перетаскивания -->
    <div id="drop-files"
         class="form-group row"
         ondragover="return false">
        <p>Перетащите изображение сюда</p>
        <label class="btn btn-primary">
            <input type="file"
                   id="uploadbtn"
                   multiple />
            <i class="icofont icofont-upload-alt"></i>
            Выберите изображение
        </label>

    </div>

    <!-- Список добавленных файлов -->
    <div class="outer_card_container">
        <div class="card">
            <div class="card-header">
                <h5>Добавленные файлы</h5>
            </div>
            <div class="card-block">
                <div class="row image_card_container">
                </div>
            </div>
        </div>
    </div>

    <div>
        <?php print_r($model->errors) ?>
    </div>

    <div class="model_loaded_images">
        <?php foreach ($model->images as $image_data):?>
        <img src="<?=$image_data?>">
        <?php endforeach; ?>
    </div>
</div>

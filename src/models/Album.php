<?php

namespace xtetis\image\models;


use yii\base\Model;
use Yii;
use xtetis\image\models\Image;

class Album extends Model
{
    public $images_limit = 8;
    /**
     * @var mixed
     */
    public $id_album;
    /**
     * @var mixed
     */
    public $album_path;

    /**
     * @var mixed
     */
    public $images = [];

    /**
     * @var mixed
     */
    public $max_width = 800;
    /**
     * @var int
     */
    public $max_height = 800;

    /**
     * @var mixed
     */
    public $errors = [];


    public function rules()
    {
        return [
            //[['images'], 'required'],
            [['images'], 'validateBase64Images'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'images' => 'Изображение',
        ];
    }

    /**
     * Добавляет картинки в альбом
     *
     * @param  array   $image_base64_list
     * @return mixed
     */
    public function addAlbum()
    {
        $ret = false;
        $this->makeAlbumId();
        $this->makeAlbumPath();
        foreach ($this->images as $image)
        {
            $image_data = $this->saveImage($image);
            if ($image_data)
            {
                $image_model           = new Image;
                $image_model->id_album = $this->id_album;
                $image_model->is_main  = 0;
                $image_model->md5      = $image_data['md5'];
                $image_model->src      = $image_data['filename'];
                $image_model->save();
                $ret = $this->id_album;
            }
        }

        return $ret;
    }

    /**
     * Генерирует ID альбома как мксимвльный id_album + 1
     */
    public function makeAlbumId()
    {
        $id_album = Image::find()->min('id_album');
        $id_album = intval($id_album) ? $id_album : 0;

        $this->id_album = $id_album + 1;
    }

    /**
     * @param $image_base64
     */
    public function saveImage($image_base64)
    {

        $image_base64_array = explode(';base64,',$image_base64);
        $image_base64 = $image_base64_array[1];

        $ret                  = [];
        $ret['md5']           = md5($image_base64);
        $ret['filename']      = $this->album_path . $ret['md5'] . '.jpg';
        $ret['filename_full'] = $_SERVER['DOCUMENT_ROOT'] . $ret['filename'];
        $ret['uploaddir']     = $_SERVER['DOCUMENT_ROOT'] . $this->album_path;
        if ((!file_exists($ret['uploaddir'])) && (!mkdir($ret['uploaddir'], 0777, true)))
        {
            return false;
        }
        if (file_exists($ret['filename_full']))
        {
            return false;
        }

        $img = imagecreatefromstring(base64_decode($image_base64));

        list($width, $height) = getimagesizefromstring(base64_decode($image_base64));
        $ratio                = $width / $height;
        if ($ratio > 1)
        {
            $resized_width  = $this->max_width; //suppose 500 is max width or height
            $resized_height = $this->max_height / $ratio;
        }
        else
        {
            $resized_width  = $this->max_width * $ratio;
            $resized_height = $this->max_height;
        }

        // Создаем пустую картинку и копируем туда оригинал в нужных пропорциях
        $resized_image = imagecreatetruecolor($resized_width, $resized_height);
        imagecopyresampled($resized_image, $img, 0, 0, 0, 0, $resized_width, $resized_height, $width, $height);

        // Сохраняем как JPG
        imagejpeg($resized_image, $ret['filename_full']);

        // Освобождаем память
        imagedestroy($img);
        imagedestroy($resized_image);

        return $ret;
    }

    /**
     * Генерирует путь к изображениям альбома
     */
    /**
     * @param $data
     */
    public function makeAlbumPath()
    {
        $album_subpath    = floor($this->id_album / 1000) . '/' . $this->id_album;
        $this->album_path = '/images/store/album/' . $album_subpath . '/';
    }

    /**
     * Функция проверяет, является ли Base64 представление картинки корректным изображением
     */
    public function validateBase64Image($data)
    {
        //die(substr ( $data , 0 , 40));
        if (!strpos($data,';base64,'))
        {
            return false;
        }

        $data_array = explode(';base64,',$data);

        if (count($data_array)!=2)
        {
            return false;
        }

        $data = $data_array[1];

        $data = base64_decode($data);
        $img = imagecreatefromstring($data);

        if (!$img)
        {
            return false;
        }

        $size = getimagesizefromstring($data);

        if (!$size || 0 == $size[0] || 0 == $size[1] || !$size['mime'])
        {
            return false;
        }

        return true;
    }

    /**
     * Функция выполняет валидацию всех картинок
     */
    /*
    public function validateImages()
    {
        foreach ($this->images as $k => $image)
        {
            if (!$this->validateBase64Image($image))
            {
                $this->errors[$k][] = 'Не является изображением';
            }
        }
    }

    */

    /**
     * Функция выполняет валидацию всех картинок
     * 
     * @param $attribute_name
     * @param $params
     */
    public function validateBase64Images(
        $attribute_name,
        $params
    )
    {
        foreach ($this->$attribute_name as $k => $image)
        {
            if (!$this->validateBase64Image($image))
            {
                $this->addError($attribute_name, 'Не является изображением');
                return false;
            }
        }

        return true;
    }

    






    /**
     * Функция присваиваает список изображений для альбома
     */
    public function setImages(array $image_base64_list = [])
    {
        $this->images = $image_base64_list;
    }

}

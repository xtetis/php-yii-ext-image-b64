<?php

namespace xtetis\image\models;

use Yii;

/**
 * This is the model class for table "{{%image}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_album Альбом
 * @property int $is_main Изображение заглавное для альбома
 * @property string $md5 md5 картинки
 * @property string $src Путь к картинке
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_album', 'is_main'], 'required'],
            [['id_album', 'is_main'], 'integer'],
            [['md5'], 'string', 'max' => 50],
            [['src'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_album' => 'Альбом',
            'is_main' => 'Изображение заглавное для альбома',
            'md5' => 'md5 картинки',
            'src' => 'Путь к картинке',
        ];
    }
}

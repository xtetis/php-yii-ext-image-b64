

var xt_input_name = $('#xtetis_imagesuploader').attr('input_name'); // Имя элемента ввода
var xt_images_limit = $('#xtetis_imagesuploader').attr('images_limit'); // Максимально допусьтмое количество картинок
var xt_model_name = $('#xtetis_imagesuploader').attr('model_name'); // Класс модели



var $ = jQuery.noConflict();

// Массив для всех изображений
var dataArray = [];

// Максимальное количество загружаемых изображений за одни раз
var maxFiles = xt_images_limit;







$(document).ready(function () {
    // В dataTransfer помещаются изображения которые перетащили в область div
    jQuery.event.addProp('dataTransfer');

    // Оповещение по умолчанию
    var errMessage = 0;

    // Кнопка выбора файлов
    var defaultUploadBtn = $('#uploadbtn');
    var outer_card_container = $('#xtetis_imagesuploader .outer_card_container');
    
    

    // Показываем ранее загруженные файлы
    $( "#xtetis_imagesuploader .model_loaded_images img" ).each(function() {
        dataArray.push({ name: '111.png', value: $(this).attr('src') });
        addImage((dataArray.length - 1));
    });



    // Метод при падении файла в зону загрузки
    $('#drop-files').on('drop', function (e) {
        // Передаем в files все полученные изображения
        var files = e.dataTransfer.files;
        // Проверяем на максимальное количество файлов
        if (files.length <= maxFiles) {
            // Передаем массив с файлами в функцию загрузки на предпросмотр
            loadInView(files);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' изображений!');
            files.length = 0; return;
        }
    });

    // При нажатии на кнопку выбора файлов
    //=======================================================================
    defaultUploadBtn.on('change', function () {
        // Заполняем массив выбранными изображениями
        var files = $(this)[0].files;
        // Проверяем на максимальное количество файлов
        if (files.length <= maxFiles) {
            // Передаем массив с файлами в функцию загрузки на предпросмотр
            loadInView(files);
            // Очищаем инпут файл путем сброса формы
            //      $('#frm').each(function(){
            //	        	    this.reset();
            //			});
            $('#uploadbtn').val('');

        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' изображений!');
            files.length = 0;
        }
    });
    //=======================================================================



    // Процедура добавления эскизов на страницу
    //==========================================================================
    function addImage(ind) {

        // Если индекс отрицательный значит выводим весь массив изображений
        if (ind < 0) {
            start = 0; end = dataArray.length;
        } else {
            // иначе только определенное изображение 
            start = ind; end = ind + 1;
        }

        // Оповещения о загруженных файлах
        if (dataArray.length == 0) {
            // Если пустой массив скрываем кнопки и всю область
            $('#upload-button').hide();
            outer_card_container.hide();
        } else if (dataArray.length == 1) {
            $('#upload-button span').html("Был выбран 1 файл");
            outer_card_container.show();
        } else {
            outer_card_container.show();
            $('#upload-button span').html(dataArray.length + " файлов были выбраны");
        }

        // Цикл для каждого элемента массива
        for (i = start; i < end; i++) {
            // размещаем загруженные изображения
            if ($('#xtetis_imagesuploader .image_card_containe > .image_card').length <= maxFiles) {
                $('#xtetis_imagesuploader .image_card_container').append(
                    '<div class="col-lg-4 col-sm-6 image_card image ins_img_' + i + '" id="img-' + i + '">' +
                    '<input type="hidden" name="'+xt_model_name+'['+xt_input_name+'][]" value="' + dataArray[i].value + '">' +
                    '<div class="thumbnail">' +
                    '<div class="thumb">' +
                    '<img src="' + dataArray[i].value + '" alt="" class="img-fluid img-thumbnail">' +
                    '</div>' +
                    '<div class="text-center" style="margin-top:3px;">' +
                    '<a href="javascript:void(0)" idx="' + i + '" class="btn btn-inverse btn__drop_image"><i class="icofont icofont-ui-delete"> Удалить</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>');

            }
        }

        return false;
    }
    //==========================================================================




    // Функция загрузки изображений на предросмотр
    //=======================================================================
    function loadInView(files) {
        // Показываем обасть предпросмотра
        $('#uploaded-holder').show();

        // Для каждого файла
        $.each(files, function (index, file) {
            console.log(files[index]);
            // Несколько оповещений при попытке загрузить не изображение
            if (!files[index].type.match('image.*')) {

                if (errMessage == 0) {
                    $('#drop-files p').html('Эй! только изображения!');
                    ++errMessage
                }
                else if (errMessage == 1) {
                    $('#drop-files p').html('Стоп! Загружаются только изображения!');
                    ++errMessage
                }
                else if (errMessage == 2) {
                    $('#drop-files p').html("Не умеешь читать? Только изображения!");
                    ++errMessage
                }
                else if (errMessage == 3) {
                    $('#drop-files p').html("Хорошо! Продолжай в том же духе");
                    errMessage = 0;
                }
                return false;
            }

            // Проверяем количество загружаемых элементов
            if ((dataArray.length + files.length) <= maxFiles) {
                // показываем область с кнопками
                $('#upload-button').css({ 'display': 'block' });
            }
            else { alert('Вы не можете загружать больше ' + maxFiles + ' изображений!'); return; }

            // Создаем новый экземпляра FileReader
            var fileReader = new FileReader();
            // Инициируем функцию FileReader
            fileReader.onload = (function (file) {

                return function (e) {
                    // Помещаем URI изображения в массив

                    var flen = file.size;
                    if (flen > (1048576 * 5)) {
                        alert('Вы не можете загружать файлы больше чем 5Mb!'); return;
                    }


                    dataArray.push({ name: file.name, value: this.result });
                    addImage((dataArray.length - 1));
                };

            })(files[index]);
            // Производим чтение картинки по URI
            fileReader.readAsDataURL(file);
        });
        return false;
    }
    //=======================================================================




    // Функция удаления всех изображений
    //=======================================================================
    function restartFiles() {

        // Установим бар загрузки в значение по умолчанию
        $('#loading-bar .loading-color').css({ 'width': '0%' });
        $('#loading').css({ 'display': 'none' });
        $('#loading-content').html(' ');

        // Удаляем все изображения на странице и скрываем кнопки
        $('#upload-button').hide();
        $('#dropped-files > .image').remove();

        $('#ph_preview > .image__zzz').remove();
        $('#ph_preview_input').val('');

        $('#uploaded-holder').hide();

        // Очищаем массив
        dataArray.length = 0;

        return false;
    }
    //=======================================================================




    /**
     * Удаление только изображения 
     */
    //=======================================================================
    $("#xtetis_imagesuploader .image_card_container").on("click", "a.btn__drop_image", function () {
        // получаем id
        var idx = $(this).attr('idx');

        // Удаляем элеент #idx
        dataArray.splice(idx, 1);

        // Удаляем старые эскизы
        $('#xtetis_imagesuploader .image_card_container').html('');

        // Обновляем эскизы в соответсвии с обновленным массивом
        addImage(-1);
    });
    //=======================================================================





    // Удалить все изображения кнопка 
    $('#dropped-files #upload-button .delete').click(restartFiles);


    // Простые стили для области перетаскивания
    $('#drop-files').on('dragenter', function () {
        $(this).css({ 'box-shadow': 'inset 0px 0px 20px rgba(0, 0, 0, 0.1)', 'border': '4px dashed #bb2b2b' });
        return false;
    });

    $('#drop-files').on('drop', function () {
        $(this).css({ 'box-shadow': 'none', 'border': '4px dashed rgba(0,0,0,0.2)' });
        return false;
    });
});

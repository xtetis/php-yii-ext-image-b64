<?php
namespace xtetis\image\assets;

use yii\web\AssetBundle;

class GalleryAsset extends AssetBundle
{
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    
    public $js = [
        'js/imagesuploader-scripts.js?v=1',
    ];

    public $css = [
        'css/imagesuploader-styles.css',
    ];
    
    public function init()
    {
        $this->sourcePath = dirname(__DIR__).'/web';
        parent::init();
    }
}
